/*
 * main.cpp
 *
 *  Created on: Apr 9, 2017
 *      Author: alex
 */

#include <vector>
#include <cmath>
#include <memory>

//#include <iostream>


//class Foo{
//    public:
//        void bar(){
//            std::cout << "Hello" << std::endl;
//        }
//};


//using t_matrix = std::vector<std::vector<float>>;
//
//class GMM
//{
//public:
//    GMM(  const std::vector<float> & i_weights
//        , const std::vector<float> & i_means
//        , const t_matrix & i_invCov
//        , float i_det_E);
//
//
//    float score(const std::vector<float> & inFeatures);
//
//private:
//
//    float calc_probability(size_t numMix, const std::vector<float> & data);
//
//    float exp_coeff(size_t numMix, const std::vector<float> & data);
//
//private:
//
//    std::vector<float> weights;
//    std::vector<float> means;
//    t_matrix invCov;
//    float det_E;
//};
//
//
//GMM::GMM(  const std::vector<float> & i_weights
//         , const std::vector<float> & i_means
//         , const t_matrix & i_invCov
//         , float i_det_E)
//    : weights(i_weights)
//    , means(i_means)
//    , invCov(i_invCov)
//    , det_E(i_det_E)
//{
//}
//
//
//float GMM::score(const std::vector<float> & inFeatures)
//{
//    float sum = 0.0f;
//
//    for(size_t i = 0; i < inFeatures.size(); ++i)
//        sum += weights[i] * calc_probability(i, inFeatures);
//
//    return std::log(sum);
//}
//
//
//float GMM::exp_coeff(size_t numMix, const std::vector<float> & data)
//{
//    std::vector<float> data_minus_mean(data);
//
//    // (x - mean)
//    for(size_t i = 0; i < data_minus_mean.size(); ++i)
//        data_minus_mean[i] -= means[numMix];
//
//    std::cout << "InConv size : " << invCov.size() << "  size 2 : " << invCov[0].size() << std::endl;
//
//    // (x - mean) * E
//    std::vector<float> resXE (invCov.size());
//    for(size_t i = 0; i < invCov.size(); ++i)
//    {
//        float sum = 0.0f;
//
//        for(size_t j = 0; j < invCov.size(); ++j)
//            sum += data_minus_mean[j] * invCov[j][i];
//
//        resXE[i] = sum;
//    }
//
//    // Res * (x - mean)
//    float res = 0.0f;
//    for(size_t i = 0; i < data_minus_mean.size(); ++i)
//        res += data_minus_mean[i] * resXE[i];
//
//    return res;
//}
//
//
//float GMM::calc_probability(size_t numMix, const std::vector<float> & data)
//{
//    std::cout << "Start to calc prob" << std::endl;
//    return 1 / std::sqrt(std::pow(2.0f * PI, static_cast<double>(data.size())) * det_E) * std::exp(-0.5 * exp_coeff(numMix, data));
//}


#include "GMM.h"


class PyGMM
{
public:

    PyGMM(  int numMix
          , int dim
          , double * weights
          , double * detE
          , double * means
          , double * invCov)
    {
//        t_vector v_weights(weights, weights + numMix);
//        t_vector v_detE(detE, detE + numMix);
//
//        t_matrix v_means;
//        for (size_t i = 0; i < numMix; ++i)
//        {
//            size_t row_size = i * dim;
//            v_means.emplace_back(means + row_size, means + row_size + dim);
//        }
//
//        t_vec_matrix v_invCov(numMix);
//        for (size_t i = 0; i < numMix; ++i)
//        {
//
//            size_t matrix_size = i * dim * dim;
//
//            for (size_t j = 0; j < dim; ++j)
//            {
//                size_t row_size = j * dim;
//                v_invCov[i].emplace_back(invCov + matrix_size + row_size, invCov + matrix_size + row_size + dim);
//            }
//        }
//
//        gmm.reset(new GMM(numMix, dim, v_weights, v_detE, v_means, v_invCov));


        NonCopyVector nv_weights(weights, numMix);
        NonCopyVector nv_detE(detE, numMix);

        nt_matrix nv_means;
        for (size_t i = 0; i < numMix; ++i)
        {
            size_t row_size = i * dim;
            nv_means.emplace_back(means + row_size, dim);
//            std::cout << "Start means : " << means[row_size] << "\n";
//
//            for (const auto & numMix : nv_means)
//            {
//                std::cout << "Mean : \n";
//                for (auto e : numMix)
//                    std::cout << e << ", ";
//                std::cout << std::endl;
//            }
        }

        nt_vec_matrix nv_invCov(numMix);
        for (size_t i = 0; i < numMix; ++i)
        {

            size_t matrix_size = i * dim * dim;

            for (size_t j = 0; j < dim; ++j)
            {
                size_t row_size = j * dim;
                nv_invCov[i].emplace_back(invCov + matrix_size + row_size, dim);
            }
        }

        gmm.reset(new GMM(numMix, dim, nv_weights, nv_detE, nv_means, nv_invCov));
    }

    double score(double * x, size_t len)
    {
        nt_vector tmp_x (x, len);

        return gmm->score(tmp_x);
    }


private:
    std::unique_ptr<GMM> gmm;
};


//void take_array(float * arr, int size)
//{
//    std::cout << "Print arr : \n";
//    for (int i = 0; i < size; ++i)
//        std::cout << arr[i] << ", ";
//    std::cout << std::endl;
//}


extern "C" {
//    Foo* Foo_new(){ return new Foo(); }
//    void Foo_bar(Foo* foo){ foo->bar(); }


//    void py_take_array(float * arr, int size)
//    { take_array(arr, size); }


    PyGMM* PyGMM_new(  int numMix
                     , int dim
                     , double * weights
                     , double * detE
                     , double * means
                     , double * invCov)
    { return new PyGMM(numMix, dim, weights, detE, means, invCov);}

    double PyGMM_score(PyGMM * obj, double * data, int len) {return obj->score(data, len);}
}

