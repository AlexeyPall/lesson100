/*
 * GMM.h
 *
 *  Created on: Apr 9, 2017
 *      Author: alex
 */

#pragma once


#include <vector>
#include <cstddef>

#include <iostream>

#define PI 3.14159265359f


/**
 * NonCopyVector
 *
 */
class NonCopyVector
{
public:
    NonCopyVector(double * i_data, size_t i_len)
        : data(i_data), len(i_len)
    {}

    double& operator()(size_t idx) { return data[idx]; }
    double operator()(size_t idx) const { return data[idx]; }

    double& operator[](size_t idx) { return data[idx]; }
    double operator[](size_t idx) const { return data[idx]; }

    size_t size() const {return len;}

    double * begin() {return data;}
    double * end() {return data + len;}

    const double * begin() const {return data;}
    const double * end() const {return data + len;}

private:
    double * data;
    size_t len;
};


/**
 * NonCopyVector
 *
 */
//class NonCopyMatrix
//{
//    NonCopyMatrix(double * i_data, size_t i_row, size_t i_col)
//        : row(i_row), col(i_col)
//    {
//        data = new double* [row];
//
//        for (size_t i = 0; i < row; ++i)
//            data[i] = i_data + i * col;
//    }
//
//    double& operator()(size_t idRow, size_t idCol) { return data[idRow][idCol]; }
//    double operator()(size_t idRow, size_t idCol) const { return data[idRow][idCol]; }
//
//    size_t get_rows() const {return row;}
//    size_t get_cols() const {return col;}
//
//
//private:
//    double ** data;
//    size_t row;
//    size_t col;
//};



using t_vector     = std::vector<double>;
using t_matrix      = std::vector<std::vector<double>>;
using t_vec_matrix  = std::vector<std::vector<std::vector<double>>>;


using nt_vector      = NonCopyVector;
using nt_matrix      = std::vector<NonCopyVector>;
using nt_vec_matrix  = std::vector<std::vector<NonCopyVector>>;


class GMM
{
public:

    GMM(  size_t i_numMix
        , size_t i_dim
        , nt_vector & i_weights
        , nt_vector & i_detE
        , nt_matrix & i_means
        , nt_vec_matrix & i_invCov
       )
        : numMix(i_numMix)
        , dim(i_dim)
        , weights(i_weights)
        , detE(i_detE)
        , means(i_means)
        , invCov(i_invCov)
    {
//        for (auto e : weights)
//            std::cout << "W : " << e << "\n";
//
//        for (const auto & mat : i_invCov)
//        {
//            std::cout << "iCov : \n";
//            for(const auto &row : mat)
//            {
//                for(auto e : row)
//                    std::cout << e << ", ";
//                std::cout << "\n";
//            }
//        }
//
//        for (const auto & numMix : means)
//        {
//            std::cout << "\nInit Mean : \n";
//            for (auto e : numMix)
//                std::cout << e << ", ";
//        }
//
//        std::cout << std::endl;

    }


    double score(nt_vector & inFeatures);

    double exp_coeff(size_t numMix, nt_vector & data);

    double calc_probability(size_t numMix, nt_vector & data);


private:
    size_t      numMix;
    size_t      dim;

    nt_vector    weights;
    nt_vector    detE;
    nt_matrix    means;
    nt_vec_matrix invCov;
};



