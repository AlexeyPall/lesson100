/*
 * GMM.cpp
 *
 *  Created on: Apr 9, 2017
 *      Author: alex
 */

#include "GMM.h"

#include <cmath>
//#include <iostream>


double GMM::score(nt_vector & inFeatures)
{
    double sum = 0.0f;

    for(size_t i = 0; i < numMix; ++i)
    {
        double a = calc_probability(i, inFeatures);
        sum += weights[i] * a;
//        std::cout << "W : " << weights[i] << " with prob : " << a << std::endl;
    }

//    std::cout << "Ret val : " << sum << " log val: " << -4.0 * std::log(sum) << std::endl;
    return std::log(sum);
}


double GMM::exp_coeff(size_t mixId, nt_vector & data)
{
     std::vector<double> data_minus_mean(&data[0], &data[0] + data.size());

//     for (auto e : data_minus_mean)
//         std::cout << e << ", ";
//     std::cout << std::endl;

//    std::cout << "Stage 1" << std::endl;


    // (x - mean)
    for(size_t j = 0; j < dim; ++j)
    {
//        std::cout << "data : " << data_minus_mean[j] << " mean : " <<  means[mixId][j] << "\n";
        data_minus_mean[j] -= means[mixId][j];
    }


//    std::cout << "Stage 2" << std::endl;
//    std::cout << "Size : " << invCov.size() << ", " << invCov[0].size() << ", " << invCov[0][0].size() << std::endl;

    // (x - mean) * E
    std::vector<double> resXE (dim);
    for(size_t i = 0; i < dim; ++i)
    {
        double sum = 0.0f;

        for(size_t j = 0; j < dim; ++j)
            sum += data_minus_mean[j] * invCov[mixId][j][i];

        resXE[i] = sum;
    }

//    std::cout << "Stage 3" << std::endl;

    // Res * (x - mean)
    double res = 0.0f;
    for(size_t i = 0; i < dim; ++i)
        res += data_minus_mean[i] * resXE[i];

//    std::cout << "uEu : " << res << std::endl;

    return res;
}


double GMM::calc_probability(size_t mixId, nt_vector & data)
{
    //std::cout << "Size : " << data.size() << " det : " << detE[mixId] << " exp : " << std::exp(-0.5 * exp_coeff(mixId, data)) << std::endl;
    return 1.0 /(std::pow(2.0f * PI, static_cast<double>(data.size()) / 2.0) * detE[mixId]) * std::exp(-0.5 * exp_coeff(mixId, data));
}



