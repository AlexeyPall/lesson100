
import os
import subprocess
import sys

from makeSortDir import MakeSortDir

from AcoModels import AcoModel
from AcoModels import AcoModelSet


PATH_TO_PROJECT = '/home/alex/projects/pycharm/ASRlesson050'
# PATH_TO_DATA_TEST = '/home/alex/lessons/ASR/decoder/test_data/data/myDaNet_etalon_test/test'
# PATH_TO_DATA_ETALONS = '/home/alex/lessons/ASR/decoder/test_data/data/myDaNet_etalon_test/etalon'
PATH_TO_MFCC_CALC = '/home/alex/projects/ASR/kaldi/src/featbin'
MFCC_CALC_EXEC = 'compute-mfcc-feats'
WORK_DIR = '/home/alex/lessons/ASR/decoder/test_data/data/VoxforgeRu/train'

class CalcMfcc:

    @staticmethod
    def get_files_list(folder_path):
        """ """

        files = dict()

        for _file in os.listdir(folder_path):
            files[_file] = folder_path + '/' + _file

        return files

    @staticmethod
    def create_mfcc_features(path_to_scp, path_to_result):

        proc = subprocess.Popen([PATH_TO_MFCC_CALC + '/' + MFCC_CALC_EXEC,
                                 'scp:' + path_to_scp,
                                 'ark,t:' + path_to_result],
                                stdout=sys.stdout,
                                stderr=sys.stderr,
                                cwd=WORK_DIR
                                )

        proc.wait()


def calc_mfcc():
    TRAIN_SCP_FILE = '/home/alex/lessons/ASR/decoder/test_data/data/VoxforgeRu/train/trainwavs.scp'
    CalcMfcc.create_mfcc_features(TRAIN_SCP_FILE, PATH_TO_PROJECT + '/VoxForge/train.txtftr')


def create_sort_dir():
    ALI_FILE = '/home/alex/lessons/ASR/decoder/test_data/data/VoxforgeRu/ali/monoali.phoneNames'
    # ALI_FILE = '/home/alex/lessons/ASR/decoder/test_data/data/VoxforgeRu/ali/test.ali'
    ARK_FILE = '/home/alex/projects/pycharm/ASRlesson050/VoxForge/train.txtftr'
    SORT_DIR = '/home/alex/projects/pycharm/ASRlesson050/VoxForge/sort_dir'

    obj = MakeSortDir(tra_file=ALI_FILE,
                      ark_file=ARK_FILE)

    obj.fill_sort_dir(sort_folder=SORT_DIR)


def train_model(new_phonems_name):
    SORT_DIR = '../ASRlesson060/data/VoxForge/sort_dir'
    PHONE_DIR = 'data/VoxForge/' + new_phonems_name

    AcoModelSet(SORT_DIR, PHONE_DIR)


def load_aco_model():
    PHONE_DIR = '/home/alex/projects/pycharm/ASRlesson050/VoxForge/ph_models'

    model = AcoModelSet.load_aco_model_set(PHONE_DIR)
    model.print_all()

if __name__ == '__main__':

    train_model('ph_models_MIX_sk_2_10_diag')


