import wave
import contextlib
import os

from os.path import isfile, join


def durations(folder_name):

    total_duration = 0.0

    if isfile(folder_name):
        with contextlib.closing(wave.open(folder_name, 'r')) as f:
            frames = f.getnframes()
            rate = f.getframerate()
            return frames / float(rate)

    onlyfiles = [join(folder_name, f) for f in os.listdir(folder_name) if isfile(join(folder_name, f))]

    for _file in onlyfiles:

        with contextlib.closing(wave.open(_file, 'r')) as f:
            frames = f.getnframes()
            rate = f.getframerate()
            duration = frames / float(rate)
            # print('File {} with duration {}'.format(_file, duration))

            total_duration += duration

    return total_duration


def calc_rtf(time_calc, folder_name):

    duration = durations(folder_name)

    print('RTF {:.2f} ({:.2f}/{:.2f})'.format(time_calc/duration, time_calc, duration))


if __name__ == '__main__':

    calc_rtf(63.313249826431274, '/home/alex/lessons/ASR/decoder/test_data/data/myDaNetSeq/wavs')
