
import subprocess
import sys
import os


PATH_TO_PROJECT = '/home/alex/projects/pycharm/ASRlesson050'
PATH_TO_DATA_TEST = '/home/alex/lessons/ASR/decoder/test_data/data/myDaNet_etalon_test/test'
PATH_TO_DATA_ETALONS = '/home/alex/lessons/ASR/decoder/test_data/data/myDaNet_etalon_test/etalon'
PATH_TO_MFCC_CALC = '/home/alex/projects/ASR/kaldi/src/featbin'
MFCC_CALC_EXEC = 'compute-mfcc-feats'


class GenerateMfcc:
    """ """

    @staticmethod
    def generate_scp(folder_name, res_filename):
        """ """

        with open(res_filename, 'w') as f:
            for _file in os.listdir(folder_name):
                f.write(_file + ' ' + folder_name + '/' + _file + '\n')

    @staticmethod
    def create_mfcc_features(path_to_scp, path_to_result):
        proc = subprocess.Popen([PATH_TO_MFCC_CALC + '/' + MFCC_CALC_EXEC,
                                 'scp:' + path_to_scp,
                                 'ark,t:' + path_to_result],
                                stdout=sys.stdout,
                                stderr=sys.stderr
                                )

        proc.wait()


#TEST

# FOLDER_WITH_FILES = '/home/alex/lessons/ASR/decoder/test_data/data/myStreetDaNet/wavs'
FOLDER_WITH_FILES = '/home/alex/lessons/ASR/decoder/test_data/data/myStreetDigits/wavs'

# SCP_STREET_DA_NET = PATH_TO_PROJECT + '/da_net/street_da_net.scp'
# MFCC_STREET_DA_NET = PATH_TO_PROJECT + '/da_net/street_da_net.txtftr'
SCP_STREET_DA_NET = PATH_TO_PROJECT + '/digits/street_digits.scp'
MFCC_STREET_DA_NET = PATH_TO_PROJECT + '/digits/street_digits.txtftr'

GenerateMfcc().generate_scp(FOLDER_WITH_FILES, SCP_STREET_DA_NET)
GenerateMfcc().create_mfcc_features(SCP_STREET_DA_NET, MFCC_STREET_DA_NET)
