import AcoModels
import numpy as np

EmptyDistValue = np.float32("inf")


class GaussMixCompCache:
    def __init__(self, acoModelSet):
        self.acoModelSet = acoModelSet
        self.ftr = None
        self.cache = np.empty(acoModelSet.getUgidCount(), dtype=np.float32)

    def reset(self):
        self.ftr = None
    pass

    def setFrame(self, ftr):
        self.ftr = ftr
        self.cache.fill(EmptyDistValue)
    pass

    def getDist(self, ph_id, ph_name):

        assert self.ftr is not None

        ugid = ph_id[ph_name]

        if self.cache[ugid] == EmptyDistValue:
            # True:
            self.cache[ugid] = self.acoModelSet.getDist(ph_name, self.ftr)
        return self.cache[ugid]
    pass
pass





